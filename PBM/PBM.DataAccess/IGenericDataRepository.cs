﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PBM.DomainModel;

namespace PBM.DataAccess
{
    public interface IGenericDataRepository<T> where T : class
    {
        //IList<T> GetAll(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] navigationProperties);
        //IList<T> GetList(Expression<Func<T, bool>> where, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] navigationProperties);
        //IList<TResult> GetProjectedList<TResult>(Expression<Func<T, TResult>> selector, Expression<Func<T, bool>> where, Func<IQueryable<TResult>, IOrderedQueryable<TResult>> orderBy = null, bool distinct = false);
        //IList<T> GetListPaging(Expression<Func<T, bool>> where, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, int numberToShow, int pageNumber, params Expression<Func<T, object>>[] navigationProperties);
        //IList<TResult> GetGroupedProjectedList<TResult, TKey>(Expression<Func<T, TKey>> groupBy, Expression<Func<IGrouping<TKey, T>, TResult>> selector, Expression<Func<T, bool>> where = null, Func<IQueryable<TResult>, IOrderedQueryable<TResult>> orderBy = null);
        //IList<TResult> GetProjectedListPaging<TResult>(Expression<Func<T, TResult>> selector, Expression<Func<T, bool>> where, Func<IQueryable<TResult>, IOrderedQueryable<TResult>> orderBy, int numberToShow, int pageNumber, bool distinct = false);
        //T GetSingle(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties);
        //int GetCount(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties);
        //int GetDistinctCount<TProperty>(Expression<Func<T, TProperty>> keySelect, Expression<Func<T, bool>> where);
        //TResult Query<TResult>(Func<IQueryable<T>, TResult> queryFunction);
        //void Add(params T[] items);
        //void Update(params T[] items);
        //void Remove(params T[] items);
    }
}
/* 
 * Note that the return type of the two Get* methods is IList<T> rather than IQueryable<T>. This means that the methods will be returning 
 * the actual already executed results from the queries rather than executable queries themselves. Creating queries and return these back 
 * to the calling code would make the caller responsible for executing the LINQ-to-Entities queries and consequently use EF logic. Besides, 
 * when using EF in an N-tier application the repository typically creates a new context and dispose it on every request meaning the calling 
 * code won’t have access to it and therefore the ability to cause the query to be executed. Thus you should always keep your LINQ queries 
 * inside of the repository when using EF in a disconnected scenario such as in an N-tier application.
*/