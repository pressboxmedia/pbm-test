﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using PBM.DomainModel;
using LinqKit;

namespace PBM.DataAccess
{
    public class GenericDataRepository<T> : IGenericDataRepository<T> where T : class
    {
        //public virtual IList<T> GetAll(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] navigationProperties)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>();

        //        foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
        //            dbQuery = dbQuery.Include<T, object>(navigationProperty);

        //        if (orderBy != null)
        //        {
        //            return orderBy(dbQuery).AsNoTracking().ToList();
        //        }
        //        else
        //        {
        //            return dbQuery.AsNoTracking().ToList();
        //        }
        //    }
        //}

        //public virtual IList<T> GetList(Expression<Func<T, bool>> where, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] navigationProperties)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>().AsExpandable();

        //        foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
        //            dbQuery = dbQuery.Include<T, object>(navigationProperty);

        //        if (where != null)
        //            dbQuery = dbQuery.Where(where);

        //        if (orderBy != null)
        //        {
        //            dbQuery = orderBy(dbQuery);
        //        }
                
        //        return dbQuery.AsNoTracking().ToList();
        //    }
        //}

        ///// <summary>
        ///// Returns a list that is projected to another result type.
        ///// </summary>
        ///// <param name="selector">Select clause used to preform the projection between the entity type and the result type.</param>
        ///// <param name="where">Predicate expression to apply to the enity object prior to being projected.</param>
        ///// <param name="orderBy">Sorting to apply to the result object after being projected.</param>
        ///// <param name="distinct">True to return only distinct projections, otherwise false. Defaults to false.</param>
        ///// <returns></returns>
        //public virtual IList<TResult> GetProjectedList<TResult>(Expression<Func<T, TResult>> selector, Expression<Func<T, bool>> where, Func<IQueryable<TResult>, IOrderedQueryable<TResult>> orderBy = null, bool distinct = false)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>().AsExpandable();

        //        if (where != null)
        //            dbQuery = dbQuery.Where(where);

        //        IQueryable<TResult> projection = dbQuery.Select(selector);

        //        if (distinct)
        //        {
        //            projection = projection.Distinct();
        //        }

        //        if (orderBy != null)
        //        {
        //            projection = orderBy(projection);
        //        }

        //        return projection.ToList();
        //    }
        //}

        ///// <summary>
        ///// Returns a list using the skip/take for paging. NOTE: the order by is required when using paging.
        ///// </summary>
        ///// <param name="where"></param>
        ///// <param name="orderBy"></param>
        ///// <param name="numberToShow"></param>
        ///// <param name="pageNumber"></param>
        ///// <param name="navigationProperties"></param>
        ///// <returns></returns>
        //public virtual IList<T> GetListPaging(Expression<Func<T, bool>> where, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, int numberToShow, int pageNumber, params Expression<Func<T, object>>[] navigationProperties)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>().AsExpandable();

        //        foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
        //            dbQuery = dbQuery.Include<T, object>(navigationProperty);

        //        if (where != null)
        //            dbQuery = dbQuery.Where(where);

        //        return orderBy(dbQuery)
        //            .Skip(numberToShow * pageNumber)
        //            .Take(numberToShow)
        //            .AsNoTracking()
        //            .ToList();
        //    }
        //}
        
        ///// <summary>
        ///// Returns a list using the skip/take for paging that is projected to another result type. NOTE: the order by is required when using paging.
        ///// </summary>
        ///// <param name="selector">Select clause used to preform the projection between the entity type and the result type.</param>
        ///// <param name="where">Predicate expression to apply to the enity object prior to being projected.</param>
        ///// <param name="orderBy">Sorting to apply to the result object after being projected.</param>
        ///// <param name="numberToShow">Number of results to return.</param>
        ///// <param name="pageNumber">Number of results to skip before building the result set.</param>
        ///// <param name="distinct">True to return only distinct projections, otherwise false. Defaults to false.</param>
        ///// <returns></returns>
        //public virtual IList<TResult> GetProjectedListPaging<TResult>(Expression<Func<T, TResult>> selector, Expression<Func<T, bool>> where, Func<IQueryable<TResult>, IOrderedQueryable<TResult>> orderBy, int numberToShow, int pageNumber, bool distinct = false)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>().AsExpandable();

        //        if (where != null)
        //            dbQuery = dbQuery.Where(where);

        //        IQueryable<TResult> projection = dbQuery.Select(selector);

        //        if (distinct)
        //        {
        //            projection = projection.Distinct();
        //        }

        //        return orderBy(projection)
        //            .Skip(numberToShow * pageNumber)
        //            .Take(numberToShow)
        //            .ToList();
        //    }
        //}
        
        ///// <summary>
        ///// Returns a list that is grouped by a key with a selcected group of columns and aggregrates
        ///// </summary>
        ///// <typeparam name="TResult"></typeparam>
        ///// <typeparam name="TKey"></typeparam>
        ///// <param name="groupBy"></param>
        ///// <param name="selector"></param>
        ///// <param name="where"></param>
        ///// <param name="orderBy"></param>
        ///// <returns></returns>
        //public virtual IList<TResult> GetGroupedProjectedList<TResult, TKey>(Expression<Func<T, TKey>> groupBy, Expression<Func<IGrouping<TKey, T>, TResult>> selector, Expression<Func<T, bool>> where = null, Func<IQueryable<TResult>, IOrderedQueryable<TResult>> orderBy = null)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>().AsExpandable();
                
        //        if (where != null) {
        //            dbQuery = dbQuery.Where(where);
        //        }

        //        IQueryable<TResult> projection = dbQuery.GroupBy(groupBy).Select(selector);

        //        if (orderBy != null)
        //        {
        //            projection = orderBy(projection);
        //        }

        //        return projection.ToList();
        //    }
        //}

        //public virtual T GetSingle(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>().AsExpandable();

        //        foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
        //            dbQuery = dbQuery.Include<T, object>(navigationProperty);

        //        return dbQuery
        //            .AsNoTracking() //Don't track any changes for the selected item
        //            .FirstOrDefault(where); //Apply where clause
        //    }
        //}

        //public int GetCount(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>();

        //        if (where != null)
        //            dbQuery = dbQuery.AsExpandable().Where(where);

        //        return dbQuery.AsNoTracking().Count();
        //    }
        //}

        ///// <summary>
        ///// Returns a count of the object filtered by the where parameter, that are distinct based on the distinctProperty parameter.
        ///// </summary>
        ///// <typeparam name="TProperty"></typeparam>
        ///// <param name="distinctProperty"></param>
        ///// <param name="where"></param>
        ///// <param name="navigationProperties"></param>
        ///// <returns></returns>
        //public int GetDistinctCount<TProperty>(Expression<Func<T, TProperty>> distinctProperty, Expression<Func<T, bool>> where)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>();
        //        return dbQuery.AsExpandable().Where(where).Select(distinctProperty).Distinct().Count();
        //    }
        //}

        //public TResult Query<TResult>(Func<IQueryable<T>, TResult> queryFunction)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        return queryFunction(context.Set<T>().AsExpandable());
        //    }
        //}

        //public virtual void Add(params T[] items)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        foreach (T item in items)
        //        {
        //            context.Entry(item).State = System.Data.Entity.EntityState.Added;
        //        }
        //        context.SaveChanges();
        //    }

        //}

        //public virtual void Update(params T[] items)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        foreach (T item in items)
        //        {
        //            context.Entry(item).State = System.Data.Entity.EntityState.Modified;
        //        }
        //        context.SaveChanges();
        //    }

        //}

        //public virtual void Remove(params T[] items)
        //{
        //    using (var context = new PBMEntities())
        //    {
        //        foreach (T item in items)
        //        {
        //            // Use Remove instead of setting the state to Deleted so cascading deletes work. In order for an item to be 
        //            // Removed it must first be Attached to the context.
        //            context.Set<T>().Attach(item);
        //            context.Set<T>().Remove(item);
        //            //context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
        //        }
        //        context.SaveChanges();
        //    }

        //}
    }
}
