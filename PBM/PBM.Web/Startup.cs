﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PBM.Web.Startup))]
namespace PBM.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
